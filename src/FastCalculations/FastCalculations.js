// FastCalculations
import React, { Component } from 'react';
import GameTitle from "../ui/GameTitle";
import Operation from "../ui/BinaryOperation";
import connect from "react-redux/es/connect/connect";

class FastCalculations extends Component {
    fieldChanged(index) {
        const self = this
        return function (field, value) {
            self.props.valueChanged(index, field, value)
        }
    }

    render() {
        const items = this.props.fastCalculations.items || []
        const tot = items.length
        const ok = items.filter(i => i.status === 'ok').length
        const newDisabled = (ok !== tot)
        return (
            <div className="col-xs-12 center-text">
                <GameTitle text="Calcolo veloce"/>
                {items.map((o, i) => <Operation key={i} status={o.status}
                                                x={o.x} y={o.y} z={o.z} op={'+'}
                                                userX={o.userX} userY={o.userY} userZ={o.userZ}
                                                fieldChanged={this.fieldChanged(i)}/>)}
                {
                    items && items.length ?
                    <div className="row">Risposte corrette: {ok} / {tot} </div> :
                    null
                }
                <div className="row">
                    <button type="button" disabled={newDisabled} className="btn btn-primary" onClick={this.props.newGame}>Nuova partita
                    </button>
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        newGame: () => dispatch({type: 'START_FAST_CALCULATIONS'}),
        valueChanged: (i, f, v) => dispatch({type: 'FC_OPERATION_VALUE_CHANGED', index: i, field: f, value: v})
    }
}

export default connect(
    state => ({fastCalculations: state.fastCalculations}),
    mapDispatchToProps
)(FastCalculations)