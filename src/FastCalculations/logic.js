import {rndInt} from '../random'

export default (state = {}, action) => {
    console.log(action.type)
    switch (action.type) {
        case 'START_FAST_CALCULATIONS': {
            const itemCount = 6
            let items = []

            const itemAlreadyPresent = function(item) {
                return items.find(it => it.x === item.x && it.y === item.y)
            }

            for (let i = 0; i < itemCount; i++) {
                let newItem = {}
                let x = 0
                let y = 0
                do {
                    x = rndInt(1, 9)
                    y = rndInt(1, 9)
                    newItem = {x, y}
                } while ((x + y <= 10) || itemAlreadyPresent(newItem))
                items.push(newItem)
            }
            return Object.assign({}, {items})
        }

        case 'FC_OPERATION_VALUE_CHANGED': {
            const processItem = function(it, field, value) {
                it[field] = value
                it.status = ((it.x || it.userX) + (it.y || it.userY) === (it.z || it.userZ) ? 'ok' : 'ko')
                return it
            }
            const items = state.items.map((it, i) => i !== action.index ? it : processItem(it, action.field, action.value))
            return Object.assign({}, state, {items})
        }

        default:
            return state
    }
}