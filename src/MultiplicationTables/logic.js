import {rndInt} from '../random'

export default (state = {}, action) => {
    console.log('MT', action.type)
    switch (action.type) {
        case 'START_MULTIPLICATION_TABLES': {
            const itemCount = 14
            let items = []

            const itemAlreadyPresent = function(item) {
                return items.find(it => it.x === item.x && it.y === item.y)
            }

            for (let i = 0; i < itemCount; i++) {
                let newItem = {}
                let x = 0
                let y = 0
                do {
                    x = rndInt(2, 10)
                    y = rndInt(2, 10)
                    newItem = {x, y}
                } while ((x * y < 10) || itemAlreadyPresent(newItem))
                items.push(newItem)
            }
            return Object.assign({}, {items})
        }

        case 'MT_OPERATION_VALUE_CHANGED': {
            const processItem = function(it, field, value) {
                it[field] = value
                it.status = ((it.x || it.userX) * (it.y || it.userY) === (it.z || it.userZ) ? 'ok' : 'ko')
                return it
            }
            const items = state.items.map((it, i) => i !== action.index ? it : processItem(it, action.field, action.value))
            return Object.assign({}, state, {items})
        }

        default:
            return state
    }
}