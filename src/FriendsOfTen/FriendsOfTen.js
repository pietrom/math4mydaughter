// FriendsOfTen
import React, {Component} from 'react';
import GameTitle from "../ui/GameTitle";
import Operation from "../ui/BinaryOperation";
import connect from "react-redux/es/connect/connect";

class FriendsOfTen extends Component {
    fieldChanged(index) {
        const self = this
        return function (field, value) {
            self.props.valueChanged(index, field, value)
        }
    }

    render() {
        const items = this.props.friendsOfTen.items || []
        const tot = items.length
        const ok = items.filter(i => i.status === 'ok').length
        const newDisabled = (ok !== tot)
        return (
            <div className="col-xs-12 center-text">
                <GameTitle text="Gli amici del dieci"/>
                {items.map((o, i) => <Operation key={i} status={o.status}
                                                x={o.x} y={o.y} z={o.z} op={'+'}
                                                userX={o.userX} userY={o.userY} userZ={o.userZ}
                                                fieldChanged={this.fieldChanged(i)}/>)}
                {
                    items && items.length ?
                        <div className="row">Risposte corrette: {ok} / {tot} </div> :
                        null
                }
                <div className="row">
                    <button type="button" disabled={newDisabled} className="btn btn-primary" onClick={this.props.newGame}>Nuova partita
                    </button>
                </div>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        newGame: () => dispatch({type: 'START_FRIENDS_OF_TEN'}),
        valueChanged: (i, f, v) => dispatch({type: 'FOT_OPERATION_VALUE_CHANGED', index: i, field: f, value: v})
    }
}

export default connect(
    state => ({friendsOfTen: state.friendsOfTen}),
    mapDispatchToProps
)(FriendsOfTen)