import {rndInt} from '../random'

export default (state = {}, action) => {
    console.log(action.type)
    switch (action.type) {
        case 'START_FRIENDS_OF_TEN': {
            const itemCount = 6
            let items = []

            const itemAlreadyPresent = function(item) {
                return items.find(it => (it.x === item.x && it.y === undefined) || (it.x === undefined && it.y === item.y))
            }

            for (let i = 0; i < itemCount; i++) {
                let rnd = 0
                let newItem = {}
                do {
                    rnd = rndInt(1, 9)
                    let pos = rndInt(1, 2)
                    newItem = pos === 1 ? {x: rnd, z:10} : {y: rnd, z:10}
                } while (itemAlreadyPresent(newItem))
                items.push(newItem)
            }
            return Object.assign({}, {items})
        }

        case 'FOT_OPERATION_VALUE_CHANGED': {
            const processItem = function(it, field, value) {
                it[field] = value
                it.status = ((it.x || it.userX) + (it.y || it.userY) === (it.z || it.userZ) ? 'ok' : 'ko')
                return it
            }
            const items = state.items.map((it, i) => i !== action.index ? it : processItem(it, action.field, action.value))
            return Object.assign({}, state, {items})
        }

        default:
            return state
    }
}