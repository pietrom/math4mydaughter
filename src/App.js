import React, { Component } from 'react';
import { HashRouter as Router, Route, Link } from 'react-router-dom'
import { Provider } from 'react-redux'
import './App.css';
import Home from './Home'
import TwinCouples from './TwinCouples/TwinCouples'
import FriendsOfTen from './FriendsOfTen/FriendsOfTen'
import CrocodileLillo from './CrocodileLillo'
import FastCalculations from './FastCalculations/FastCalculations'
import MultiplicationTables from './MultiplicationTables/MultiplicationTables'

class App extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <Router>
          <div className="container">

          <nav className="navbar navbar-default">
            <div className="container-fluid">
              <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
                
              </div>
              <div id="navbar" className="navbar-collapse collapse">
                <ul className="nav navbar-nav">
                  <li className="active"><Link to="/">Home</Link></li>
                  <li className="dropdown">
                    <Link to="/" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gioca! <span className="caret"></span></Link>
                    <ul className="dropdown-menu">
                      <li><Link to="/twin-couples">Le coppie gemelle</Link></li>
                      <li><Link to="/friends-of-ten">Gli amici del dieci</Link></li>
                      <li><Link to="/crocodile-lillo">Il coccodrillo Lillo</Link></li>
                      <li><Link to="/fast-calculations">Calcolo veloce</Link></li>
                      <li><Link to="/multiplication-tables">Tabelline</Link></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
          <div className="text-center">
            <Route exact path="/" component={Home} />
            <Route exact path="/twin-couples" component={TwinCouples} />
            <Route exact path="/friends-of-ten" component={FriendsOfTen} />
            <Route exact path="/crocodile-lillo" component={CrocodileLillo} />
            <Route exact path="/fast-calculations" component={FastCalculations} />
            <Route exact path="/multiplication-tables" component={MultiplicationTables} />
          </div>
        </div>        
        </Router>
      </Provider>
    );
  }
}

export default App;
