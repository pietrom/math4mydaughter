import React, { Component } from 'react';
import GameTitle from "./ui/GameTitle";

class Home extends Component {
  render() {
    return (
        <div className="col-xs-12 center-text">
            <GameTitle text="Gioca con la matematica!" />Scegli un gioco ed inizia a divertirti...
	    </div>
    );
  }
}

export default Home;