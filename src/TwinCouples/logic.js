import {rndInt} from '../random'

export default (state = {}, action) => {
    switch (action.type) {
        case 'START_TWIN_COUPLES': {
            const itemCount = 6
            let items = []
            for (let i = 0; i < itemCount; i++) {
                let rnd = 0
                do {
                    rnd = rndInt(1, 10)
                } while (items.indexOf(rnd) >= 0)
                items.push(rnd)
            }
            const buildItem = function(n) {
                return {x: n, y: n}
            }
            items = items.map(buildItem)
            return Object.assign({}, {items})
        }

        case 'TC_OPERATION_VALUE_CHANGED': {
            const processItem = function(it, field, value) {
                it[field] = value
                it.status = ((it.x || it.userX) + (it.y || it.userY) === (it.z || it.userZ) ? 'ok' : 'ko')
                return it
            }
            const items = state.items.map((it, i) => i !== action.index ? it : processItem(it, action.field, action.value))
            return Object.assign({}, state, {items})
        }

        default:
            return state
    }
}