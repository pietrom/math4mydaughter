import React, { Component } from 'react'
import './BinaryOperation.css'

function ItemValue(props) {
    return <span>{props.value}</span>
}

function ItemInput(props) {
    return <input className="form-control text-center" type="number" value={props.value || ''} onChange={props.valueChanged} />
}

const Item = function(props) {
    return props.value ? <ItemValue value={props.value} /> : <ItemInput value={props.userValue} valueChanged={props.valueChanged} />
}

const backgrounds = {
    "ok" : "ok",
    "ko" : "ko"
}

class BinaryOperation extends Component {
    valueChanged(field) {
        const self = this
        return function(event) {
            try {
                const strValue = event.target.value
                if(strValue && strValue.trim()) {
                    const value = parseInt(strValue)
                    self.props.fieldChanged(field, value)
                } else {
                    self.props.fieldChanged(field, null)
                }
            } catch(err) {
                console.log(err)
            }
        }
    }
    render() {
        const props = this.props
        const className = `row operation ${backgrounds[props.status] || 'undecided'}`
        return (
            <div className={className}>
                <div className="col-xs-1"></div>
                <div className="col-xs-2 text-center"><Item value={props.x} userValue={props.userX} valueChanged={this.valueChanged('userX')} /></div>
                <div className="col-xs-2 text-center">{props.op}</div>
                <div className="col-xs-2 text-center"><Item value={props.y} userValue={props.userY} valueChanged={this.valueChanged('userY')} /></div>
                <div className="col-xs-2 text-center">=</div>
                <div className="col-xs-2 text-center"><Item value={props.z} userValue={props.userZ} valueChanged={this.valueChanged('userZ')} /></div>
                <div className="col-xs-1"></div>
            </div>
        )
    }
}

export default BinaryOperation