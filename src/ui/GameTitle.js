import React from "react"

function GameTitle(props) {
    return <div className="jumbotron">
        <h1>{props.text}</h1>
    </div>
}

export default GameTitle