import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createStore, combineReducers } from 'redux'
import reducers from './reducers'
import twinCouples from './TwinCouples/logic'
import friendsOfTen from './FriendsOfTen/logic'
import fastCalculations from './FastCalculations/logic'
import multiplicationTables from './MultiplicationTables/logic'
import * as serviceWorker from './serviceWorker';

const store = createStore(combineReducers({ twinCouples, friendsOfTen, fastCalculations, multiplicationTables, reducers }))

ReactDOM.render(<App store={store}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
